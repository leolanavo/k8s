#!/bin/bash

minikube start --memory 8192 --cpus 4 --vm-driver kvm2

helm init

helm repo add istio.io https://storage.googleapis.com/istio-prerelease/daily-build/master-latest-daily/charts

helm repo update

while [ "$STATUS" != "Running" ]; do
  sleep 1
  STATUS=$(kubectl get pods --namespace kube-system -l app=helm,name=tiller -o jsonpath='{.items[?(@)].status.phase}')
done

NAMESPACE="istio-system"

helm install istio.io/istio-init --namespace=$NAMESPACE

helm install istio.io/istio --name istio --namespace=$NAMESPACE

echo "All done!!"